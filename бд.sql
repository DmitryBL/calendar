CREATE TABLE IF NOT EXISTS `Test` ( 
	`id` int(10) unsigned NOT NULL AUTO_INCREMENT, 
	`top` varchar(128) NOT NULL, 
	`type` varchar(25) NOT NULL, 
	`location` varchar(128) NOT NULL, 
	`date` varchar(25) NOT NULL, 
	`time` varchar(25) NOT NULL, 
	`duration` varchar(25) NOT NULL, 
	`comment` varchar(255) NOT NULL,
	`complated` tinyint(1) NOT NULL,
	`hide`		tinyint(1) DEFAULT 0,
	
	PRIMARY KEY (`id`) 
);