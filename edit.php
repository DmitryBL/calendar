<?php
require_once 'connection.php';

$n_id = $_GET['id'];
$note = mysqli_query($link, "Select * from `Test` Where `id`='$n_id'");
$note = mysqli_fetch_assoc($note);
?>

<!DOCTYPE html>
<html lang='ru'>

<head>
    <meta charset='utf-8'>
    <title>Редактирование</title>
    <link href="css.css" rel="stylesheet">
</head>
<H1>Редактировать задачу</H1>
<body>
   <div class="forma">
    <form method="POST" action="endEdit.php">
      <input type="hidden" name="id" value="<?= $note['id'] ?>">
    	<label>Тема:</label>
          <br>
          <input type="text" name="Top" value="<?= $note['top'] ?>">
          <br>
    	<label>Тип:</label>
          <br>
          <select name="Type">
            <option <?= $note['type']==='встреча'?' selected':'' ?>>встреча</option>
            <option <?= $note['type']==='звонок'?' selected':'' ?>>звонок</option>
            <option <?= $note['type']==='совещание'?' selected':'' ?>>совещание</option>
    		    <option <?= $note['type']==='дело'?' selected':'' ?>>дело</option>
          </select>
          <br>
    	<label>Место:</label>
          <br>
          <input type="text" name="Location" value="<?= $note['location'] ?>">
          <br>
    	<label>Дата и время:</label>
          <br>
      	  <input type="date" name="Date" class="date" value="<?= $note['date'] ?>">
    	  <input type="time" name="Time" class="time" value="<?= $note['time'] ?>">
    	  <br>
        <label>Длительность:</label>
          <br>
          <select name="Duration" value="<?= $note['duration'] ?>">
            <option>0:30</option>
            <option>1:00</option>
            <option>1:30</option>
    		    <option>2:00</option>
    		    <option>2:30</option>
          </select>
    	  <br>
          <label>Комментарий:</label>
          <br>
          <textarea name="Comment" rows="5"><?= $note['comment'] ?></textarea>
          <br>
          <label>Выполнено:</label>
          <input type="checkbox" name="Complated" <?= $note['complated'] === '1' ? ' checked' : '' ?>>
          <br>
          <button type="submit">Сохранить</button>
    </form>
    <form type="button" action="index.php">
      <button >Назад</button>
    </form>
  </div>
  <p>
  
</body>

</html>