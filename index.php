<?php
  require_once 'connection.php';
  include 'vals.php';
?>
<!DOCTYPE html>
<html lang='ru'>

<head>
    <meta charset='utf-8'>
    <title>Календарь</title>
    <link href="css.css" rel="stylesheet">
</head>
<H1>Мой календарь</H1>
<body>
  <div class="forma">
    <form method="POST" action="send.php">
    
    	<h3>Новая Задача</h3>
    	
    	<label>ЗАДАЧА:</label>
      <br>
      <input style="width: 400px" type="text" name="Top">
      <br>
    	
    	<label>Тип:</label>
      <br>
      <select name="Type">
        <option>встреча</option>
        <option>звонок</option>
        <option>совещание</option>
    		<option>дело</option>
      </select>
      <br>
    	
    	<label>Место:</label>
      <br>
      <input style="width: 400px" type="text" name="Location">
      <br>
    	
    	<label>Дата и время:</label>
      <br>
  	  <input type="date" name="Date" class="date" value="2021-06-15">
  	  <input type="time" name="Time" class="time" value="12:00">
  	  <br>
    	
      <label>Длительность:</label>
      <br>
      <select name="Duration">
        <option >0:30</option>
        <option >1:00</option>
        <option >1:30</option>
      	<option >2:00</option>
      	<option >2:30</option>
      </select>
      <br>

      <label>Комментарий:</label>
      <br>
      <textarea name="Comment" rows="5"></textarea>
      <br>
      <label>Выполнено:</label>
      <input type="checkbox" name="Complated">
      <br>
      <button type="submit">Добавить</button>

    </form>
  </div>
  <br>

  <div class="forma" >
    <h3>Список задач</h3>
    <form method="POST" action="index.php">
      <label>Выполненость:</label>
      <select name="selector">
        <option value="all" <?= $_POST['selector'] === 'all' ? ' selected' : '' ?>>Все</option>
        <option value="current" <?= $_POST['selector'] === 'current' ? ' selected' : '' ?>>Текущие задачи</option>
        <option value="overdue" <?= $_POST['selector'] === 'overdue' ? ' selected' : '' ?>>Просроченные задачи</option>
        <option value="complated" <?= $_POST['selector'] === 'complated' ? ' selected' : '' ?>>Выполненые задачи</option>
      </select>
      <!--<br>-->
      <label>Использовать дату:</label>
      <input type="checkbox" name="complated_filter" <?= $_POST['complated_filter'] === "on" ? ' checked' : '' ?>>
      <input type="date" name="date_filter" class="date" value="<?= $_POST['date_filter']?>">

      <br>
      <label>В течении скольки дней:</label>
      <input style="width: 30px" type="text" name="durdom" value="<?= $_POST['durdom']?>">

      <button type="submit">Обновить</button>
    </form>
  
    
    <?php
      $ddtmp =  intval($_POST['durdom']);
      $prSrt = "SELECT * FROM `Test` ";
      if($_POST['selector'] === 'all'){
        $prSrt .=  "WHERE TRUE ";
      }
      if($_POST['selector'] === 'current'){
        $prSrt .= "WHERE CURDATE()<`date` AND `complated` = 0 ";
      }
      if($_POST['selector'] === 'overdue'){
        $prSrt .= "WHERE CURDATE()>`date` AND `complated` = 0 ";
      }
      if($_POST['selector'] === 'complated'){
        $prSrt .=  "WHERE `complated` = 1 ";
      }

      if($_POST['complated_filter'] === "on"){
        #$_POST['date_filter']
        $prSrt .= 'AND `date` >= ' . "'" . $_POST['date_filter'] . "' AND `date` < " . "('" . $_POST['date_filter'] . "' + INTERVAL " . $ddtmp . " DAY)";
      }
      else {

      }

      $prSrt .= " AND `hide` = 0;";
      #echo $prSrt;
      $notes = mysqli_query($link, $prSrt);
      
      $notes = mysqli_fetch_all($notes);
    ?>

    <table class="table">
      <tr>
        <!--<th scope="col">id</th>-->
        <th scope="col">Задача</th>
        <th scope="col">Тип</th>
        <th scope="col">Место</th>
        <th scope="col">Дата</th>
        <th scope="col">Время</th>
        <th scope="col">Длительность</th>
        <th scope="col">Статус</th>
        <th scope="col">Модификация:</th>
        <!--<th scope="col">Удалить</th>-->
      </tr>
      
      <?php foreach($notes as $note) { ?>
      
      <tr>
        <!--<td><?=$note[0] ?></td>-->
        <td><?=$note[1] ?></td>
        <td><?=$note[2] ?></td>
        <td><?=$note[3] ?></td>
        <td><?=$note[4]?></td>
        <td><?=$note[5] ?></td>
        <td><?=$note[6] ?></td>
        <!--<td><?=$note[7] ?></td> Комментарий-->
        <td><?=$note[8] ?></td>
        <td><a href="edit.php?id=<?= $note[0] ?>">EDIT</a>
        <a style="color:red;" href="delete.php?id=<?= $note[0] ?>">DEL</a>
        </td>
        <!--<td><a style="color:red;" href="delete.php?id=<?= $note[0] ?>">DEL</a></td>-->
      </tr>
      
      <?php
      }
      ?>
    
    </table>

  </div>
</body>

</html>